package util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import util.DbUtil;

public class DbUtil {
	private String dbUrl = "jdbc:mysql://localhost:3306/test?serverTimezone=GMT";
	private String dbUserName = "root";
	private String dbPassword = "123";
	private String jdbcName = "com.mysql.cj.jdbc.Driver";
	
	//获取数据库连接
	public Connection getCon(){
		try {
			Class.forName(jdbcName);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Connection con = null;
		try {
			con = DriverManager.getConnection(dbUrl, dbUserName, dbPassword);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return con;
	}
	//关闭数据库连接
	public void closeCon(Connection con)throws Exception{
		if(con!=null){
			con.close();
		}
	}
	
	public static void main(String[] args) {
		DbUtil dbUtil=new DbUtil();
		try {
			dbUtil.getCon();
			System.out.println("数据库连接成功！");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("数据库连接失败");
		}
	}

}
