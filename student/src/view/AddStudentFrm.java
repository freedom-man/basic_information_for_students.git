package view;

import java.awt.BorderLayout;


import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import dao.StudentDao;
import model.Student;
import util.StringUtil;

import javax.swing.ButtonGroup;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JButton;
import javax.swing.JRadioButton;
import javax.swing.JComboBox;
import javax.swing.JSeparator;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;

public class AddStudentFrm extends JInternalFrame {

	private JPanel contentPane;
	private JTextField studentNameTextField;
	private JTextField studentIdTextField;
	private JTextField studentPasswordTextField;
	private JTextField studentDoTextField;
	private JTextField studentHoTextField;
	private JTextField studentPhTextField;
	private JTextField studentDayTextField;
	private JTextField studentPoTextField;
	private JTextField studentClasstextField;
	private JTextField studentSexTextField;



	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AddStudentFrm frame = new AddStudentFrm();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public AddStudentFrm() {
		setTitle("\u6DFB\u52A0\u5B66\u751F");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 665, 467);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		setClosable(true);
		setIconifiable(true);
		
		JLabel label = new JLabel("\u5B66\u751F\u59D3\u540D\uFF1A");
		label.setFont(new Font("΢���ź�", Font.PLAIN, 14));
		
		studentNameTextField = new JTextField();
		studentNameTextField.setColumns(10);
		
		JLabel label_1 = new JLabel("\u5B66\u53F7\uFF1A");
		label_1.setFont(new Font("΢���ź�", Font.PLAIN, 14));
		
		JLabel label_2 = new JLabel("\u767B\u5F55\u5BC6\u7801\uFF1A");
		label_2.setFont(new Font("΢���ź�", Font.PLAIN, 14));
		
		JLabel label_3 = new JLabel("\u6027\u522B\uFF1A");
		label_3.setFont(new Font("΢���ź�", Font.PLAIN, 14));
		
		JLabel label_4 = new JLabel("\u51FA\u751F\u65E5\u671F\uFF1A");
		label_4.setFont(new Font("΢���ź�", Font.PLAIN, 14));
		
		JLabel label_5 = new JLabel("\u653F\u6CBB\u9762\u8C8C\uFF1A");
		label_5.setFont(new Font("΢���ź�", Font.PLAIN, 14));
		
		JLabel label_6 = new JLabel("\u5BB6\u5EAD\u4F4F\u5740\uFF1A");
		label_6.setFont(new Font("΢���ź�", Font.PLAIN, 14));
		
		JLabel label_7 = new JLabel("\u5BBF\u820D\u53F7\uFF1A");
		label_7.setFont(new Font("΢���ź�", Font.PLAIN, 14));
		
		JLabel label_8 = new JLabel("\u7535\u8BDD\uFF1A");
		label_8.setFont(new Font("΢���ź�", Font.PLAIN, 14));
		
		studentIdTextField = new JTextField();
		studentIdTextField.setColumns(10);
		
		studentPasswordTextField = new JTextField();
		studentPasswordTextField.setColumns(10);
		
		studentDoTextField = new JTextField();
		studentDoTextField.setColumns(10);
		
		studentHoTextField = new JTextField();
		studentHoTextField.setColumns(10);
		
		studentPhTextField = new JTextField();
		studentPhTextField.setColumns(10);
		
		studentDayTextField = new JTextField();
		studentDayTextField.setColumns(10);
		
		studentPoTextField = new JTextField();
		studentPoTextField.setColumns(10);
		
		
		JButton resetButton = new JButton("\u91CD\u7F6E");
		resetButton.setIcon(new ImageIcon(AddStudentFrm.class.getResource("/images/\u91CD\u7F6E.png")));
		resetButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				/*studentNameTextField.setText("");
				studentIdTextField.setText("");
				studentDayTextField.setText("");
				studentPoTextField.setText("");
				studentHoTextField.setText("");
				studentDoTextField.setText("");
				studentClasstextField.setText("");
				studentPasswordTextField.setText("");
				studentPhTextField.setText("");
				sexButtonGroup.clearSelection();
				studentRadioButton.setSelected(true);*/
				resetValue(e);

			}
		});
		resetButton.setFont(new Font("΢���ź�", Font.PLAIN, 14));
		
		JButton confirmButton = new JButton("\u786E\u8BA4");
		confirmButton.setIcon(new ImageIcon(AddStudentFrm.class.getResource("/images/\u786E\u8BA4.png")));
		confirmButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				studentAddAct(e);
			}
		});
		confirmButton.setFont(new Font("΢���ź�", Font.PLAIN, 14));
		
		JSeparator separator = new JSeparator();
		
		JLabel label_9 = new JLabel("\u73ED\u7EA7\uFF1A");
		label_9.setFont(new Font("΢���ź�", Font.PLAIN, 14));
		
		studentClasstextField = new JTextField();
		studentClasstextField.setColumns(10);
		
		studentSexTextField = new JTextField();
		studentSexTextField.setColumns(10);
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(134)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(label_7, GroupLayout.PREFERRED_SIZE, 58, GroupLayout.PREFERRED_SIZE)
							.addContainerGap())
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
								.addGroup(gl_contentPane.createSequentialGroup()
									.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
										.addGroup(gl_contentPane.createSequentialGroup()
											.addGap(10)
											.addComponent(label_8, GroupLayout.PREFERRED_SIZE, 58, GroupLayout.PREFERRED_SIZE))
										.addComponent(label_6, GroupLayout.PREFERRED_SIZE, 85, GroupLayout.PREFERRED_SIZE))
									.addGap(31)
									.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING, false)
										.addComponent(studentHoTextField)
										.addComponent(studentPhTextField, GroupLayout.DEFAULT_SIZE, 155, Short.MAX_VALUE))
									.addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
								.addGroup(gl_contentPane.createSequentialGroup()
									.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
										.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
											.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
												.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
													.addGroup(gl_contentPane.createSequentialGroup()
														.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
															.addComponent(label)
															.addComponent(label_1, GroupLayout.PREFERRED_SIZE, 58, GroupLayout.PREFERRED_SIZE)
															.addComponent(label_9, GroupLayout.PREFERRED_SIZE, 58, GroupLayout.PREFERRED_SIZE))
														.addGap(42))
													.addGroup(gl_contentPane.createSequentialGroup()
														.addComponent(label_2, GroupLayout.DEFAULT_SIZE, 112, Short.MAX_VALUE)
														.addPreferredGap(ComponentPlacement.RELATED)))
												.addGroup(gl_contentPane.createSequentialGroup()
													.addComponent(label_3, GroupLayout.PREFERRED_SIZE, 58, GroupLayout.PREFERRED_SIZE)
													.addGap(58)))
											.addGroup(gl_contentPane.createSequentialGroup()
												.addComponent(label_5)
												.addGap(44)))
										.addGroup(gl_contentPane.createSequentialGroup()
											.addComponent(label_4, GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE)
											.addPreferredGap(ComponentPlacement.RELATED)))
									.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
										.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING, false)
											.addComponent(studentPoTextField, Alignment.LEADING)
											.addComponent(studentDoTextField, Alignment.LEADING)
											.addComponent(studentDayTextField, Alignment.LEADING)
											.addComponent(studentPasswordTextField, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 155, Short.MAX_VALUE))
										.addGroup(gl_contentPane.createSequentialGroup()
											.addPreferredGap(ComponentPlacement.RELATED)
											.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
												.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING, false)
													.addComponent(studentSexTextField, Alignment.LEADING)
													.addComponent(studentNameTextField, Alignment.LEADING)
													.addComponent(studentIdTextField, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 153, Short.MAX_VALUE)
													.addComponent(studentClasstextField))
												.addComponent(separator, GroupLayout.PREFERRED_SIZE, 1, GroupLayout.PREFERRED_SIZE))))
									.addGap(79)))
							.addGap(559))))
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(166)
					.addComponent(confirmButton, GroupLayout.PREFERRED_SIZE, 97, GroupLayout.PREFERRED_SIZE)
					.addGap(45)
					.addComponent(resetButton, GroupLayout.DEFAULT_SIZE, 97, Short.MAX_VALUE)
					.addGap(638))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(label_9)
						.addComponent(studentClasstextField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(15)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(label)
						.addComponent(studentNameTextField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(label_1)
						.addComponent(studentIdTextField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(separator, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addGap(2)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(label_3)
						.addComponent(studentSexTextField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(label_2)
						.addComponent(studentPasswordTextField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(studentDayTextField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(label_4))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(label_5)
						.addComponent(studentPoTextField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(10)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(label_7)
						.addComponent(studentDoTextField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(studentHoTextField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(label_6))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(studentPhTextField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(label_8))
					.addGap(18)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING, false)
						.addComponent(resetButton, 0, 0, Short.MAX_VALUE)
						.addComponent(confirmButton, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
					.addContainerGap(72, Short.MAX_VALUE))
		);
		contentPane.setLayout(gl_contentPane);
	}

	protected void resetValue(ActionEvent e) {
		// TODO Auto-generated method stub
		studentNameTextField.setText("");
		studentIdTextField.setText("");
		studentDayTextField.setText("");
		studentPoTextField.setText("");
		studentHoTextField.setText("");
		studentDoTextField.setText("");
		studentClasstextField.setText("");
		studentPasswordTextField.setText("");
		studentPhTextField.setText("");
		studentSexTextField.setText("");
		//sexButtonGroup.clearSelection();
		//studentRadioButton.setSelected(true);
	}

	protected void studentAddAct(ActionEvent e) {
		// TODO Auto-generated method stub
		String studentName = studentNameTextField.getText().toString();
		String studentPassword = studentPasswordTextField.getText().toString();
		String studentId = studentIdTextField.getText().toString();
		String studentDay = studentDayTextField.getText().toString();
		String studentPo = studentPoTextField.getText().toString();
		String studentHo = studentHoTextField.getText().toString();
		String studentPh = studentPhTextField.getText().toString();
		String studentDo = studentDoTextField.getText().toString();
		String studentClass = studentClasstextField.getText().toString();
		String studentSex = studentSexTextField.getText().toString();
		if(StringUtil.isEmpty(studentName)){
			JOptionPane.showMessageDialog(this, "����дѧ������!");
			return;
		}
		if(StringUtil.isEmpty(studentPassword)){
			JOptionPane.showMessageDialog(this, "����д����!");
			return;
		}
		if(StringUtil.isEmpty(studentId)){
			JOptionPane.showMessageDialog(this, "����дѧ��!");
			return;
		}
		if(StringUtil.isEmpty(studentDay)){
			JOptionPane.showMessageDialog(this, "����д��������!");
			return;
		}
		if(StringUtil.isEmpty(studentPo)){
			JOptionPane.showMessageDialog(this, "����д������ò!");
			return;
		}
		if(StringUtil.isEmpty(studentHo)){
			JOptionPane.showMessageDialog(this, "����д��ͥסַ!");
			return;
		}
		if(StringUtil.isEmpty(studentPh)){
			JOptionPane.showMessageDialog(this, "����д�绰!");
			return;
		}
		if(StringUtil.isEmpty(studentDo)){
			JOptionPane.showMessageDialog(this, "����д�����!");
			return;
		}
		if(StringUtil.isEmpty(studentClass)){
			JOptionPane.showMessageDialog(this, "����д�༶!");
			return;
		}
		if(StringUtil.isEmpty(studentSex)){
			JOptionPane.showMessageDialog(this, "����дѧ���Ա�!");
			return;
		}
		
		Student student = new Student();
		student.setName(studentName);
		student.setId(studentId);
		student.setPassword(studentPassword);
		student.setSex(studentSex);
		student.setBirthDay(studentDay);
		student.setDormitoryNumber(studentDo);
		student.setClassId(studentClass);
		student.setHomeAddress(studentHo);
		student.setPoliticalStatus(studentPo);
		student.setPhoneNumber(studentPh);
		
		StudentDao studentDao = new StudentDao();
		if(studentDao.addStudent(student)){
			JOptionPane.showMessageDialog(this, "���ӳɹ�!");
		}else{
			JOptionPane.showMessageDialog(this, "����ʧ��!");
		}
		resetValue(e);
	}

}
