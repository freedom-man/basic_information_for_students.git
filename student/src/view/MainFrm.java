package view;

import java.awt.BorderLayout;
import java.awt.Desktop;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URI;
import java.net.URISyntaxException;

import javax.swing.DefaultDesktopManager;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;


import model.UserType;
import javax.swing.JDesktopPane;
import java.awt.Color;
import java.awt.Font;

public class MainFrm extends JFrame {

	private JPanel contentPane;
	private JDesktopPane desktopPane;
	public static UserType userType;
	public static Object userObject;
	private JMenuItem addStudentMenuItem ;

	/**
	 * Launch the application.
	 */
//	public static void main(String[] args) {
//		EventQueue.invokeLater(new Runnable() {
//			public void run() {
//				try {
//					MainFrm frame = new MainFrm();
//					frame.setVisible(true);
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//		}
//		});
//	}

	/**
	 * Create the frame.
	 */
	public MainFrm(UserType userType,Object userObject) {
		this.userType = userType;
		this.userObject = userObject;
		
		setTitle("\u5B66\u751F\u4FE1\u606F\u7CFB\u7EDF\u4E3B\u754C\u9762");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 984, 668);
		
		JMenuBar menuBar = new JMenuBar();
		menuBar.setToolTipText("");
		setJMenuBar(menuBar);
		
		JMenu mnNewMenu = new JMenu("\u7CFB\u7EDF\u8BBE\u7F6E");
		mnNewMenu.setIcon(new ImageIcon(MainFrm.class.getResource("/images/\u7CFB\u7EDF\u8BBE\u7F6E.png")));
		menuBar.add(mnNewMenu);
		
		JMenuItem menuItem = new JMenuItem("\u4FEE\u6539\u5BC6\u7801");
		menuItem.setIcon(new ImageIcon(MainFrm.class.getResource("/images/\u4FEE\u6539\u5BC6\u7801.png")));
		menuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				editPassword(e);
				
			}
			
		});
		mnNewMenu.add(menuItem);
		
		JMenuItem mntmNewMenuItem = new JMenuItem("\u9000\u51FA\u7CFB\u7EDF");
		mntmNewMenuItem.setIcon(new ImageIcon(MainFrm.class.getResource("/images/\u9000\u51FA\u7CFB\u7EDF.png")));
		mntmNewMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (JOptionPane.showConfirmDialog(MainFrm.this, "确定退出系统？") == JOptionPane.OK_OPTION){
					System.exit(0);
		}
			}
		});
		
		JMenuItem reloadMenuItem = new JMenuItem("\u91CD\u65B0\u767B\u5F55");
		reloadMenuItem.setIcon(new ImageIcon(MainFrm.class.getResource("/images/\u91CD\u65B0\u5F00\u59CB.png")));
		reloadMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				reloadSystem(e);
			}
		});
		mnNewMenu.add(reloadMenuItem);
		mnNewMenu.add(mntmNewMenuItem);
		
		JMenu menu = new JMenu("\u5B66\u751F\u7BA1\u7406");
		menu.setIcon(new ImageIcon(MainFrm.class.getResource("/images/\u5B66\u751F\u7BA1\u7406,\u5934\u50CF,\u4EBA.png")));
		menuBar.add(menu);
		
		JMenuItem addStudentMenuItem = new JMenuItem("\u5B66\u751F\u6DFB\u52A0");
		addStudentMenuItem.setIcon(new ImageIcon(MainFrm.class.getResource("/images/\u6DFB\u52A0.png")));
		addStudentMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AddStudentFrm addStudentFrm = new AddStudentFrm();
				addStudentFrm.setVisible(true);
				desktopPane.add(addStudentFrm);
			}
		});
		menu.add(addStudentMenuItem);
		
		JMenuItem menuItem_2 = new JMenuItem("\u5B66\u751F\u5217\u8868");
		menuItem_2.setIcon(new ImageIcon(MainFrm.class.getResource("/images/\u5217\u8868.png")));
		menuItem_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				StudentManageFrm studentManageFrm = new StudentManageFrm();
				studentManageFrm.setVisible(true);
				desktopPane.add(studentManageFrm);
			}
		});
		menu.add(menuItem_2);
		
		JMenu menu_1 = new JMenu("\u73ED\u7EA7\u7BA1\u7406");
		menu_1.setIcon(new ImageIcon(MainFrm.class.getResource("/images/\u73ED\u7EA7\u7BA1\u7406.png")));
		menuBar.add(menu_1);
		
		JMenuItem menuItem_3 = new JMenuItem("\u73ED\u7EA7\u6DFB\u52A0");
		menuItem_3.setIcon(new ImageIcon(MainFrm.class.getResource("/images/\u6DFB\u52A0.png")));
		menu_1.add(menuItem_3);
		
		JMenuItem menuItem_4 = new JMenuItem("\u73ED\u7EA7\u5217\u8868");
		menuItem_4.setIcon(new ImageIcon(MainFrm.class.getResource("/images/\u5217\u8868.png")));
		menu_1.add(menuItem_4);
		
		JMenu menu_2 = new JMenu("\u5E2E\u52A9");
		menu_2.setIcon(new ImageIcon(MainFrm.class.getResource("/images/\u5E2E\u52A9.png")));
		menuBar.add(menu_2);
		
		JMenuItem menuItem_5 = new JMenuItem("\u5173\u4E8E\u6211\u4EEC");
		menuItem_5.setIcon(new ImageIcon(MainFrm.class.getResource("/images/\u5173\u4E8E\u6211\u4EEC.png")));
		menuItem_5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				aboutUs(e);
			}
			
		});
		menu_2.add(menuItem_5);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		desktopPane = new JDesktopPane();
		desktopPane.setBackground(Color.LIGHT_GRAY);
		contentPane.add(desktopPane, BorderLayout.CENTER);
		setLocationRelativeTo(null);
		//setAuthority();
		if("学生".equals(userType.getName())){
			addStudentMenuItem.setEnabled(false);
			
		}
	}

	protected void reloadSystem(ActionEvent e) {
		// TODO Auto-generated method stub
		this.dispose();
		new LoginFrm().setVisible(true);
	}

	protected void editPassword(ActionEvent e) {
		// TODO Auto-generated method stub
		EditPasswordFrm editPasswordFrm = new EditPasswordFrm();
		editPasswordFrm.setVisible(true);
		desktopPane.add(editPasswordFrm);
	}

	protected void aboutUs(ActionEvent e) {
		// TODO Auto-generated method stub
		String info = "    网络1712\n";
		info += "彭希，王洪尉制作\n";
		//JOptionPane.showMessageDialog(this, info);
		String[] buttons = {"想去看看！","暂时不想去！"};
		int ret = JOptionPane.showOptionDialog(this, info, "关于我们", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.DEFAULT_OPTION, new ImageIcon(LoginFrm.class.getResource("/images/\u5B66\u751F.png")), buttons, null);
		if(ret == 0) {
			//采用java调用系统浏览器打开指定网址
			try {
				URI uri = new URI("https://www.baidu.com");
				Desktop.getDesktop().browse(uri);
				//Runtime.getRuntime().exec("explorer https://www.cnblogs.com/PENGXI/");
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			//JOptionPane.showMessageDialog(this, "迫不及待");
		}else {
			JOptionPane.showMessageDialog(this, "滚蛋");
		}
	}
}
