package view;

import java.awt.BorderLayout;


import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JScrollPane;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.util.List;
import java.util.Vector;

import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import dao.StudentDao;
import model.Student;
import util.StringUtil;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.ImageIcon;


public class StudentManageFrm extends JInternalFrame {

	private JPanel contentPane;
	private JTextField searchStudentNameTextField;
	private JTextField searchStudentClasstextField;
	private JTable studentListTable;
	private JTextField editStudentNameTextField;
	private JTextField editStudentPasswordTextField;
	private JTextField editStudentClassTextField;
	private JTextField editStudentIdTextField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					StudentManageFrm frame = new StudentManageFrm();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public StudentManageFrm() {
		
		setTitle("\u5B66\u751F\u4FE1\u606F\u7BA1\u7406");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 853, 547);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		setIconifiable(true);
		setClosable(true);

		JScrollPane scrollPane = new JScrollPane();
		
		JLabel label = new JLabel("\u5B66\u751F\u59D3\u540D\uFF1A");
		label.setIcon(new ImageIcon(StudentManageFrm.class.getResource("/images/\u59D3\u540D.png")));
		label.setFont(new Font("΢���ź�", Font.PLAIN, 14));
		
		searchStudentNameTextField = new JTextField();
		searchStudentNameTextField.setColumns(10);
		
		JLabel label_1 = new JLabel("\u73ED\u7EA7\uFF1A");
		label_1.setIcon(new ImageIcon(StudentManageFrm.class.getResource("/images/\u73ED\u7EA7.png")));
		label_1.setFont(new Font("΢���ź�", Font.PLAIN, 14));
		
		searchStudentClasstextField = new JTextField();
		searchStudentClasstextField.setColumns(10);
		
		JButton searchButton = new JButton("\u67E5\u8BE2");
		searchButton.setIcon(new ImageIcon(StudentManageFrm.class.getResource("/images/\u67E5\u8BE2.png")));
		searchButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				searchStudent(e);

			}
		});
		searchButton.setFont(new Font("΢���ź�", Font.PLAIN, 14));
		
		JLabel label_2 = new JLabel("\u5B66\u751F\u59D3\u540D\uFF1A");
		label_2.setIcon(new ImageIcon(StudentManageFrm.class.getResource("/images/\u59D3\u540D.png")));
		label_2.setFont(new Font("΢���ź�", Font.PLAIN, 14));
		
		JLabel label_3 = new JLabel("\u767B\u5F55\u5BC6\u7801\uFF1A");
		label_3.setIcon(new ImageIcon(StudentManageFrm.class.getResource("/images/\u5BC6\u7801.png")));
		label_3.setFont(new Font("΢���ź�", Font.PLAIN, 14));
		
		JLabel lblNewLabel = new JLabel("\u73ED\u7EA7\uFF1A");
		lblNewLabel.setIcon(new ImageIcon(StudentManageFrm.class.getResource("/images/\u73ED\u7EA7.png")));
		lblNewLabel.setFont(new Font("΢���ź�", Font.PLAIN, 14));
		
		JLabel lblNewLabel_1 = new JLabel("\u5B66\u53F7\uFF1A");
		lblNewLabel_1.setIcon(new ImageIcon(StudentManageFrm.class.getResource("/images/id card.png")));
		lblNewLabel_1.setFont(new Font("΢���ź�", Font.PLAIN, 14));
		
		editStudentNameTextField = new JTextField();
		editStudentNameTextField.setColumns(10);
		
		editStudentPasswordTextField = new JTextField();
		editStudentPasswordTextField.setColumns(10);
		
		editStudentClassTextField = new JTextField();
		editStudentClassTextField.setColumns(10);
		
		editStudentIdTextField = new JTextField();
		editStudentIdTextField.setColumns(10);
		
		JButton submitStudentButton = new JButton("\u786E\u8BA4\u4FEE\u6539");
		submitStudentButton.setIcon(new ImageIcon(StudentManageFrm.class.getResource("/images/\u786E\u8BA4.png")));
		submitStudentButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				submitEditAct(e);
			}
		});
		submitStudentButton.setFont(new Font("΢���ź�", Font.PLAIN, 14));
		
		JButton deleteStudentButton = new JButton("\u5220\u9664");
		deleteStudentButton.setIcon(new ImageIcon(StudentManageFrm.class.getResource("/images/\u5220\u9664.png")));
		deleteStudentButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				deleteStudent(e);
			}
		});
		deleteStudentButton.setFont(new Font("΢���ź�", Font.PLAIN, 14));
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(85)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 643, GroupLayout.PREFERRED_SIZE)
							.addContainerGap())
						.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
							.addGroup(gl_contentPane.createSequentialGroup()
								.addPreferredGap(ComponentPlacement.RELATED)
								.addComponent(label)
								.addGap(18)
								.addComponent(searchStudentNameTextField, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
								.addGap(22)
								.addComponent(label_1, GroupLayout.PREFERRED_SIZE, 77, GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(ComponentPlacement.RELATED)
								.addComponent(searchStudentClasstextField, GroupLayout.PREFERRED_SIZE, 117, GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(ComponentPlacement.RELATED, 111, Short.MAX_VALUE)
								.addComponent(searchButton, GroupLayout.PREFERRED_SIZE, 97, GroupLayout.PREFERRED_SIZE)
								.addGap(98))
							.addGroup(gl_contentPane.createSequentialGroup()
								.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
									.addComponent(label_2)
									.addComponent(label_3))
								.addGap(31)
								.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
									.addGroup(gl_contentPane.createSequentialGroup()
										.addComponent(editStudentPasswordTextField, GroupLayout.PREFERRED_SIZE, 118, GroupLayout.PREFERRED_SIZE)
										.addGap(85))
									.addGroup(Alignment.LEADING, gl_contentPane.createSequentialGroup()
										.addComponent(editStudentNameTextField, GroupLayout.PREFERRED_SIZE, 118, GroupLayout.PREFERRED_SIZE)
										.addGap(85)))
								.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
									.addComponent(lblNewLabel)
									.addComponent(lblNewLabel_1, GroupLayout.DEFAULT_SIZE, 109, Short.MAX_VALUE))
								.addPreferredGap(ComponentPlacement.RELATED)
								.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
									.addComponent(editStudentIdTextField, GroupLayout.DEFAULT_SIZE, 131, Short.MAX_VALUE)
									.addComponent(editStudentClassTextField, GroupLayout.DEFAULT_SIZE, 131, Short.MAX_VALUE))
								.addGap(18)
								.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING, false)
									.addComponent(deleteStudentButton, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
									.addComponent(submitStudentButton, GroupLayout.DEFAULT_SIZE, 123, Short.MAX_VALUE))
								.addGap(19)))))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(55)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
						.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
							.addComponent(searchButton)
							.addComponent(label))
						.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
							.addComponent(label_1)
							.addComponent(searchStudentNameTextField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(searchStudentClasstextField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 276, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
							.addComponent(label_2)
							.addComponent(editStudentNameTextField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(lblNewLabel)
							.addComponent(editStudentClassTextField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addComponent(submitStudentButton))
					.addGap(41)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(label_3)
						.addComponent(editStudentPasswordTextField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(editStudentIdTextField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(deleteStudentButton)
						.addComponent(lblNewLabel_1))
					.addContainerGap(22, Short.MAX_VALUE))
		);
		
		studentListTable = new JTable();
		studentListTable.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				selectedTableRow(e);
			}
		});
		studentListTable.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"\u5B66\u53F7", "\u59D3\u540D", "\u73ED\u7EA7", "\u6027\u522B", "\u653F\u6CBB\u9762\u8C8C", "\u51FA\u751F\u65E5\u671F", "\u5BB6\u5EAD\u4F4F\u5740", "\u767B\u5F55\u5BC6\u7801", "\u5BBF\u820D\u53F7", "\u7535\u8BDD"
			}
		) {
			boolean[] columnEditables = new boolean[] {
				false, true, true, true, true, true, true, true, true, true
			};
			public boolean isCellEditable(int row, int column) {
				return columnEditables[column];
			}
		});
		scrollPane.setViewportView(studentListTable);
		contentPane.setLayout(gl_contentPane);
		
		if("ѧ��".equals(MainFrm.userType.getName())){
			deleteStudentButton.setEnabled(false);
		}
	}
	
	protected void deleteStudent(ActionEvent e) {	
		// TODO Auto-generated method stub
		int row = studentListTable.getSelectedRow();
		if(row == -1){
			JOptionPane.showMessageDialog(this, "��ѡ��Ҫɾ�������ݣ�");
			return;
		}
		if(JOptionPane.showConfirmDialog(this, "��ȷ��ɾ��ô��") != JOptionPane.OK_OPTION){
			return;
		}
		StudentDao studentDao = new StudentDao();
		if(studentDao.delete(Integer.parseInt(studentListTable.getValueAt(row, 0).toString()))){
			JOptionPane.showMessageDialog(this, "ɾ���ɹ���");
		}else{
			JOptionPane.showMessageDialog(this, "ɾ��ʧ�ܣ�");
		}
		studentDao.closeDao();
		setTable(new Student());
	}

	protected void selectedTableRow(MouseEvent e) {
		// TODO Auto-generated method stub
		DefaultTableModel dft = (DefaultTableModel) studentListTable.getModel();
		editStudentNameTextField.setText(dft.getValueAt(studentListTable.getSelectedRow(), 1).toString());
		editStudentPasswordTextField.setText(dft.getValueAt(studentListTable.getSelectedRow(), 7).toString());
		editStudentClassTextField.setText(dft.getValueAt(studentListTable.getSelectedRow(), 2).toString());
		editStudentIdTextField.setText(dft.getValueAt(studentListTable.getSelectedRow(), 0).toString());
		
	}

	protected void submitEditAct(ActionEvent e) {
		// TODO Auto-generated method stub
		int row = studentListTable.getSelectedRow();
		if(row == -1){
			JOptionPane.showMessageDialog(this, "��ѡ��Ҫ�޸ĵ����ݣ�");
			return;
		}
		String studentName = editStudentNameTextField.getText().toString();
		String studentPassword = editStudentPasswordTextField.getText().toString();
		String studentId = editStudentIdTextField.getText().toString();
		String studentClass = editStudentClassTextField.getText().toString();
		if(StringUtil.isEmpty(studentName)){
			JOptionPane.showMessageDialog(this, "����дѧ��������");
			return;
		}
		if(StringUtil.isEmpty(studentPassword)){
			JOptionPane.showMessageDialog(this, "����д���룡");
			return;
		}
		if(StringUtil.isEmpty(studentId)){
			JOptionPane.showMessageDialog(this, "����дѧ��ѧ�ţ�");
			return;
		}if(StringUtil.isEmpty(studentClass)){
			JOptionPane.showMessageDialog(this, "����дѧ���༶��");
			return;
		}
		
		Student student = new Student();
		student.setName(studentName);
		student.setPassword(studentPassword);
		student.setId(studentId);
		student.setClassId(studentClass);
		StudentDao studentDao = new StudentDao();
		if(studentDao.update(student)){
			JOptionPane.showMessageDialog(this, "���³ɹ���");
		}else{
			JOptionPane.showMessageDialog(this, "����ʧ�ܣ�");
		}
		studentDao.closeDao();
		setTable(new Student());
		
	}

	protected void searchStudent(ActionEvent e) {
		// TODO Auto-generated method stub
		Student student = new Student();
		student.setName(searchStudentNameTextField.getText().toString());
		student.setClassId(searchStudentClasstextField.getText().toString());
		setTable(student);
	}

	private void setTable(Student student){
		if("ѧ��".equals(MainFrm.userType.getName())){
			Student s = (Student)MainFrm.userObject;
			student.setName(s.getName());
		}
		DefaultTableModel dft = (DefaultTableModel) studentListTable.getModel();
		dft.setRowCount(0);
		StudentDao studentDao = new StudentDao();
		List<Student> studentList = studentDao.getStudentList(student);
		for (Student s : studentList) {
			Vector v = new Vector();
			v.add(s.getId());
			v.add(s.getName());
			v.add(s.getClassId());
			v.add(s.getSex());
			v.add(s.getPoliticalStatus());
			v.add(s.getBirthDay());
			v.add(s.getHomeAddress());
			v.add(s.getPassword());
			v.add(s.getDormitoryNumber());
			v.add(s.getPhoneNumber());
			dft.addRow(v);
		}
		studentDao.closeDao();
	}
	
}
